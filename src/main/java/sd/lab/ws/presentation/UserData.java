package sd.lab.ws.presentation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;

import java.io.IOException;
import java.util.Objects;
import java.util.UUID;

@JacksonXmlRootElement(localName = "user")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserData extends Data {

    private UUID id = null;
    private String username = null;
    private String fullName = null;
    private String email = null;
    private String password = null;
    private LinkData link = null;
    private Role role = null;

    public enum Role implements Comparable<Role> {
        USER("user"),
        ADMIN("admin");

        private String value;


        Role(String value) {
            this.value = value;
        }

        @Override
        @JsonValue
        @JacksonXmlText
        public String toString() {
            return value;
        }

    }

    public UserData() {

    }

    public UserData(UserData clone) {
        this.id = clone.id;
        this.username = clone.username;
        this.fullName = clone.fullName;
        this.email = clone.email;
        this.link = ifNonNull(clone.link, LinkData::new);
        this.role = clone.role;
    }

    public UserData(UUID id, String username, String fullName, String email, LinkData link, Role role) {
        this.id = id;
        this.username = username;
        this.fullName = fullName;
        this.email = email;
        this.link = link;
        this.role = role;
    }

    @JsonProperty("id")
    @JacksonXmlProperty(localName = "id")
    public UUID getId() {
        return id;
    }

    public UserData setId(UUID id) {
        this.id = id;
        return this;
    }


    @JsonProperty("username")
    @JacksonXmlProperty(localName = "username")
    public String getUsername() {
        return username;
    }

    public UserData setUsername(String username) {
        this.username = username;
        return this;
    }


    @JsonProperty("fullName")
    @JacksonXmlProperty(localName = "fullName")
    public String getFullName() {
        return fullName;
    }

    public UserData setFullName(String fullName) {
        this.fullName = fullName;
        return this;
    }


    @JsonProperty("email")
    @JacksonXmlProperty(localName = "email")
    public String getEmail() {
        return email;
    }

    public UserData setEmail(String email) {
        this.email = email;
        return this;
    }

    @JsonProperty("password")
    @JacksonXmlProperty(localName = "password")
    public String getPassword() {
        return password;
    }

    public UserData setPassword(String password) {
        this.password = password;
        return this;
    }


    @JsonProperty("link")
    @JacksonXmlProperty(localName = "link")
    public LinkData getLink() {
        return link;
    }

    public UserData setLink(LinkData link) {
        this.link = link;
        return this;
    }

    public UserData setLinkUrl(String link) {
        this.link = new LinkData(link);
        return this;
    }


    @JsonProperty("role")
    @JacksonXmlProperty(localName = "role")
    public Role getRole() {
        return role;
    }

    public UserData setRole(Role role) {
        this.role = role;
        return this;
    }

    public UserData setPropertiesToNonNullsOf(UserData other) {
        assignIfNonNull(other::getId, this::setId);
        assignIfNonNull(other::getEmail, this::setEmail);
        assignIfNonNull(other::getFullName, this::setFullName);
        assignIfNonNull(other::getLink, this::setLink);
        assignIfNonNull(other::getPassword, this::setPassword);
        assignIfNonNull(other::getRole, this::setRole);
        assignIfNonNull(other::getUsername, this::setUsername);
        return this;
    }

    public boolean sameUserOf(UserData user) {
        return user != null && (
                (id != null && Objects.equals(id, user.id))
                        || (username != null && Objects.equals(username, user.username))
                        || (email != null && Objects.equals(email, user.email))
                        || (link != null && Objects.equals(link, user.link))
        );
    }

    public boolean isIdentifiedBy(String identifier) {
        return identifier != null && (
                (id != null && Objects.equals(id.toString(), identifier))
                        || Objects.equals(username, identifier)
                        || Objects.equals(email, identifier)
                        || (link != null && Objects.equals(link.getUrl(), identifier))
        );
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserData user = (UserData) o;
        return Objects.equals(id, user.id) &&
                Objects.equals(username, user.username) &&
                Objects.equals(fullName, user.fullName) &&
                Objects.equals(email, user.email) &&
                Objects.equals(link, user.link) &&
                Objects.equals(role, user.role);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username, fullName, email, link, role);
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", fullName='" + fullName + '\'' +
                ", email='" + email + '\'' +
                ", link='" + link + '\'' +
                ", role=" + role +
                ", password='" + password + '\'' +
                '}';
    }

    public static UserData fromJSON(String representation) throws IOException {
        return Data.fromJSON(representation, UserData.class);
    }

    public static UserData fromYAML(String representation) throws IOException {
        return Data.fromYAML(representation, UserData.class);
    }

    public static UserData fromXML(String representation) throws IOException {
        return Data.fromXML(representation, UserData.class);
    }

    public static UserData parse(String mimeType, String payload) throws IOException {
        return parse(mimeType, payload, UserData.class);
    }

}
