package sd.lab.ws.presentation;

public final class MIMETypes {

    public static String APPLICATION_JSON = "application/json";

    public static String APPLICATION_YAML = "application/yaml";

    public static String APPLICATION_XML = "application/xml";

    public static String TEXT_HTML = "text/html";

    public static String TEXT_PLAIN = "text/plain";

    public static String ANY = "*/*";

    public static String APPLICATION_ANY = "application/*";
}
