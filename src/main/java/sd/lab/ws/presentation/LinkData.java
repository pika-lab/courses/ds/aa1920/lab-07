package sd.lab.ws.presentation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.IOException;
import java.util.Objects;

@JacksonXmlRootElement(localName = "link")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class LinkData extends Data {

    private String url = null;

    public LinkData() {

    }

    public LinkData(String url) {
        this.url = url;
    }

    public LinkData(LinkData clone) {
        this(clone.url);
    }

    @JsonProperty("url")
    @JacksonXmlProperty(localName = "url")
    public String getUrl() {
        return url;
    }

    public LinkData setUrl(String url) {
        this.url = url;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LinkData link = (LinkData) o;
        return Objects.equals(url, link.url);
    }

    @Override
    public int hashCode() {
        return Objects.hash(url);
    }

    @Override
    public String toString() {
        return "Link{" +
                "url='" + url + '\'' +
                '}';
    }

    public static LinkData fromJSON(String representation) throws IOException {
        return Data.fromJSON(representation, LinkData.class);
    }

    public static LinkData fromYAML(String representation) throws IOException {
        return Data.fromYAML(representation, LinkData.class);
    }

    public static LinkData fromXML(String representation) throws IOException {
        return Data.fromXML(representation, LinkData.class);
    }

    public static LinkData parse(String mimeType, String payload) throws IOException {
        return parse(mimeType, payload, LinkData.class);
    }

}
