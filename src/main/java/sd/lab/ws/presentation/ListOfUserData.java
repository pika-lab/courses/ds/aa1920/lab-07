package sd.lab.ws.presentation;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlRootElement;

import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.stream.Stream;

@JacksonXmlRootElement(localName = "listOfUsers")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ListOfUserData extends ListData<UserData> {

    public ListOfUserData() {
    }

    public ListOfUserData(Collection<? extends UserData> collection) {
        super(collection);
    }

    public ListOfUserData(Stream<? extends UserData> stream) {
        super(stream);
    }

    public ListOfUserData(UserData element1, UserData... elements) {
        super(element1, elements);
    }

    @JsonProperty("users")
    @JacksonXmlElementWrapper(useWrapping = false)
    @JacksonXmlProperty(localName = "user")
    public List<UserData> getUsers() {
        return getItems();
    }

    public ListOfUserData setUsers(List<UserData> users) {
        setItems(users);
        return this;
    }

    public static ListOfUserData fromJSON(String representation) throws IOException {
        return Data.fromJSON(representation, ListOfUserData.class);
    }

    public static ListOfUserData fromYAML(String representation) throws IOException {
        return Data.fromYAML(representation, ListOfUserData.class);
    }

    public static ListOfUserData fromXML(String representation) throws IOException {
        return Data.fromXML(representation, ListOfUserData.class);
    }

    public static ListOfUserData parse(String mimeType, String payload) throws IOException {
        return parse(mimeType, payload, ListOfUserData.class);
    }
}
