package sd.lab.ws;

import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import io.vertx.core.*;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonObject;
import io.vertx.core.json.jackson.DatabindCodec;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import sd.lab.ws.routes.LindaPath;
import sd.lab.ws.routes.Path;

import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Service extends AbstractVerticle {
    private static final Logger LOGGER = LoggerFactory.getLogger(Service.class);
    private static final int DEFAULT_PORT = 8080;
    
    private Router router;
    private HttpServer server;
    private final CompletableFuture<Service> deployment = new CompletableFuture<>();
    private final CompletableFuture<Void> termination  = new CompletableFuture<>();

    @Override
    public void init(Vertx vertx, Context context) {
        super.init(vertx, context);
        router = Router.router(vertx);
    }

    @Override
    public void start(Future<Void> startFuture) {
        DatabindCodec.mapper().registerModule(new JavaTimeModule());

        attach(router, new LindaPath("1"));
        
        server = getVertx().createHttpServer()
	        .requestHandler(router::accept)
	        .listen(getPort(), x -> {
	            if (x.succeeded()) {
                    LOGGER.info("Service listening on port: {0}", "" + getPort());
                    startFuture.complete();
                    deployment.complete(this);
                } else {
                    LOGGER.info("Failure in starting the server on port {0}", "" + getPort());
                    startFuture.fail(x.cause());
                    deployment.completeExceptionally(x.cause());
                }
            });

    }

    @Override
    public void start() throws Exception {
        start(Future.future());
    }

    @Override
    public void stop(Future<Void> stopFuture) {
        server.close(x -> {
            if (x.succeeded()) {
                LOGGER.info("Service is not listening anymore");
                stopFuture.complete();
                termination.complete(null);
            } else {
                LOGGER.info("Failure in shutting down the service");
                stopFuture.fail(x.cause());
                termination.completeExceptionally(x.cause());
            }
        });
    }

    public void stop() {
        stop(Future.future());
    }

    private int getPort() {
    	final JsonObject config = context.config();
    	if (config != null && config.containsKey("port")) {
    		return config.getInteger("port");
    	} else {
    		return DEFAULT_PORT;
    	}
    }

    private void attach(Router router, Path path) {
        path.attach(router);
    }

    public void awaitTermination() throws ExecutionException, InterruptedException {
        termination.get();
    }

    public void awaitTermination(Duration wait) throws ExecutionException, InterruptedException, TimeoutException {
        termination.get(wait.toMillis(), TimeUnit.MILLISECONDS);
    }

    public Service awaitDeployment() throws ExecutionException, InterruptedException {
        return deployment.get();
    }

    public Service awaitDeployment(Duration wait) throws ExecutionException, InterruptedException, TimeoutException {
        return deployment.get(wait.toMillis(), TimeUnit.MILLISECONDS);
    }

    public static Service start(String... args) {
        final Vertx vertx = Vertx.vertx();
        return start(vertx, args);
    }

    public static Service start(Vertx vertx, String... args) {
        final JsonObject config = parserArgs(args);
        final Service service = new Service();
        vertx.deployVerticle(service, new DeploymentOptions(config));
        return service;
    }

    public static void main(String... args) throws InterruptedException, ExecutionException {
        start(args).awaitDeployment();
    }

    private static JsonObject parserArgs(String... args) {
        final List<String> arguments = Arrays.asList(args);
        final int pIndex = arguments.indexOf("-p");
        final Optional<String> portString = Optional.ofNullable(pIndex >= 0 ? arguments.get(pIndex + 1) : null);

        JsonObject config = new JsonObject();
        config.put("port", portString.map(Integer::parseInt).orElse(DEFAULT_PORT));
        return new JsonObject().put("config", config);
    }

}