package sd.lab.ws.routes;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Route;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.ErrorHandler;
import io.vertx.ext.web.handler.LoggerHandler;
import sd.lab.ws.auth.AuthSupport;
import sd.lab.ws.exceptions.BadContentError;
import sd.lab.ws.exceptions.HttpError;
import sd.lab.ws.presentation.Data;
import sd.lab.ws.presentation.UserData;
import sd.lab.ws.storage.UserStorage;

import java.util.Collection;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;

public abstract class Path implements AuthSupport {

	private static final Logger LOGGER = LoggerFactory.getLogger(Path.class);

	private String path;
	private UserStorage userStorage;
	private Router router;
    private String parentPath = "";

	public Path(String subPath, UserStorage userStorage) {
		this.path = Objects.requireNonNull(subPath);
		this.userStorage = Objects.requireNonNull(userStorage);
	}

	protected abstract void setupRoutes();

	@Override
	public UserStorage getUserStorage() {
		return userStorage;
	}

	protected Route addRoute(HttpMethod method, String path, Handler<RoutingContext> handler) {
		final String fullPath = getPath() + path;

		LOGGER.info("Setup route {0} {1}", method, fullPath);
		return router.route(method, fullPath)
	        .handler(LoggerHandler.create())
	    	.handler(BodyHandler.create())
	    	.handler(handler)
	    	.handler(ErrorHandler.create());
    }

	protected Route addRoute(HttpMethod method, Handler<RoutingContext> handler) {
		return addRoute(method, "", handler);
	}

	protected String getPath() {
		return parentPath + path;
	}

	protected String getSubPath(String subResource) {
		return getPath() + "/" + subResource;
	}

    protected  <X extends Data> Handler<AsyncResult<X>> responseHandler(RoutingContext routingContext) {
	    return responseHandler(routingContext, Function.identity());
    }

	protected  <X extends Data> Handler<AsyncResult<X>> responseHandler(RoutingContext routingContext, Function<X, X> cleaner) {
		return x -> {
			if (x.failed() && x.cause() instanceof HttpError) {
				final HttpError exception = (HttpError) x.cause();
				routingContext.response()
						.setStatusCode(exception.getStatusCode())
						.end(exception.getMessage());
			} else if (x.failed()) {
				routingContext.response()
						.setStatusCode(500)
						.end("Internal Server Error");

			} else {
			    try {
                    final X cleanResult = cleaner.apply(x.result());
                    String result = cleanResult.toMIMETypeString(routingContext.getAcceptableContentType());
                    routingContext.response()
                            .putHeader(HttpHeaders.CONTENT_TYPE, routingContext.getAcceptableContentType())
                            .setStatusCode(200)
                            .end(result);
                } catch (HttpError e) {
                    routingContext.response()
                            .setStatusCode(e.getStatusCode())
                            .end(e.getMessage());
                } catch (Exception e) {
			    	routingContext.response()
							.setStatusCode(500)
							.end("Internal Server Error");
				}
			}
		};
	}

	protected Handler<AsyncResult<Void>> responseHandlerWithNoContent(RoutingContext routingContext) {
		return x -> {
			if (x.failed() && x.cause() instanceof HttpError) {
				final HttpError exception = (HttpError) x.cause();
				routingContext.response()
						.setStatusCode(exception.getStatusCode())
						.end(exception.getMessage());
			} else if (x.failed()) {
				routingContext.response()
						.setStatusCode(500)
						.end("Internal Server Error");

			} else {
				routingContext.response()
						.setStatusCode(204)
						.end();
			}
		};
	}

	public void attach(Router router) {
        this.router = Objects.requireNonNull(router);
        setupRoutes();
    }

    public Path append(Path subPath) {
        subPath.parentPath = path;
        subPath.attach(router);
        return subPath;
    }

    public Path append(String prefix, Path subPath) {
        subPath.parentPath = parentPath + path + prefix;
        subPath.attach(router);
        return subPath;
    }

    protected static void requireAllAreNull(Object x, Object... xs) {
        if (Stream.concat(Stream.of(x), Stream.of(xs)).anyMatch(Objects::nonNull)) {
            throw new BadContentError();
        }
    }

    protected static void requireAllAreNullOrEmpty(Collection<?> x, Collection<?>... xs) {
        if (Stream.concat(Stream.of(x), Stream.of(xs))
                .filter(Objects::nonNull).anyMatch(it -> it.size() > 0)) {
            throw new BadContentError();
        }
    }

    protected static void requireNoneIsNull(Object x, Object... xs) {
        if (Stream.concat(Stream.of(x), Stream.of(xs)).anyMatch(Objects::isNull)) {
            throw new BadContentError();
        }
    }

    protected static void requireSomeIsNonNull(Object x, Object... xs) {
        if (!Stream.concat(Stream.of(x), Stream.of(xs)).anyMatch(Objects::nonNull)) {
            throw new BadContentError();
        }
    }

    protected static <X> void  requireParameterIsPresent(Optional<X> parameter) {
		if (!parameter.isPresent()) {
			throw new BadContentError();
		}
	}

	protected static void  requireStringIsNotBlank(Optional<String> parameter) {
		requireParameterIsPresent(parameter);
		if (parameter.get().trim().isEmpty()) {
			throw new BadContentError();
		}
	}

	protected static <X> void  requireOneOf(X object, X... possibleValues) {
		if (!Stream.of(possibleValues).anyMatch(it -> Objects.equals(it, object))) {
			throw new BadContentError();
		}
	}

    protected Optional<UserData> enrichUser(UserData user) {
		return userStorage.find(user);
    }

	protected Optional<UserData> findUser(String identifier) {
		return userStorage.findByIdentifier(identifier);
	}
}
