package sd.lab.ws.routes;

import io.vertx.core.Promise;
import io.vertx.core.http.HttpMethod;
import io.vertx.ext.web.RoutingContext;
import sd.lab.ws.api.UsersApi;
import sd.lab.ws.exceptions.BadContentError;
import sd.lab.ws.exceptions.HttpError;
import sd.lab.ws.presentation.LinkData;
import sd.lab.ws.presentation.ListOfUserData;
import sd.lab.ws.presentation.UserData;
import sd.lab.ws.storage.UserStorage;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

import static sd.lab.ws.presentation.MIMETypes.*;

public class UsersPath extends Path {


    public UsersPath(UserStorage userStorage) {
        super("/users", userStorage);
    }

	@Override
    // TODO understand this
	protected void setupRoutes() {

        addRoute(HttpMethod.POST, this::postUser)
                .consumes(APPLICATION_JSON)
                .consumes(APPLICATION_XML)
                .consumes(APPLICATION_YAML)
                .produces(APPLICATION_JSON)
                .produces(APPLICATION_XML)
                .produces(APPLICATION_YAML);

        addRoute(HttpMethod.GET, this::getUsers)
                .produces(APPLICATION_JSON)
                .produces(APPLICATION_XML)
                .produces(APPLICATION_YAML);

        addRoute(HttpMethod.GET, "/:identifier", this::getUser)
                .produces(APPLICATION_JSON)
                .produces(APPLICATION_XML)
                .produces(APPLICATION_YAML);

        addRoute(HttpMethod.PUT, "/:identifier", this::putUser)
                .consumes(APPLICATION_JSON)
                .consumes(APPLICATION_XML)
                .consumes(APPLICATION_YAML)
                .produces(APPLICATION_JSON)
                .produces(APPLICATION_XML)
                .produces(APPLICATION_YAML);
	}

    // TODO notice: this is a stub
	private void postUser(RoutingContext routingContext) {
		final UsersApi api = UsersApi.get(routingContext, getUserStorage());
        final Promise<LinkData> result = Promise.promise();

        result.future().setHandler(responseHandler(routingContext, this::cleanPostResult)); // TODO notice: cleaning output

		try {
			final UserData user = UserData.parse(routingContext.parsedHeaders().contentType().value(), routingContext.getBodyAsString());
            validateUserForPost(user); // TODO notice: validating input

			api.createUser(user, result); // TODO notice: forwarding business logic to UsersApi
		} catch(HttpError e) {
            result.fail(e);
        } catch (IOException | IllegalArgumentException e) {
			result.fail(new BadContentError(e));
		}
	}

    private LinkData cleanPostResult(LinkData x) {
        return new LinkData(getPath() + x.getUrl());
    }

    private void validateUserForPost(UserData user) {
        requireNoneIsNull(user.getEmail(), user.getUsername(), user.getPassword());
        requireAllAreNull(user.getId(), user.getLink());

        user.setId(UUID.randomUUID());
        user.setLinkUrl(getSubPath(user.getUsername()));
    }

    private void getUsers(RoutingContext routingContext) {
        final UsersApi api = UsersApi.get(routingContext, getUserStorage());
        final Promise<ListOfUserData> result = Promise.promise();
        result.future().setHandler(responseHandler(routingContext, this::cleanUsers));

        try {
            final Optional<Integer> skip = Optional.ofNullable(routingContext.queryParams().get("skip")).map(Integer::parseInt);
            final Optional<Integer> limit = Optional.ofNullable(routingContext.queryParams().get("limit")).map(Integer::parseInt);
            final Optional<String> filter = Optional.ofNullable(routingContext.queryParams().get("filter"));

            // TODO forward business logic to UsersApi
            throw new IllegalStateException("not implemented");
        } catch(HttpError e) {
            result.fail(e);
        } catch (IllegalArgumentException e) {
            result.fail(new BadContentError(e));
        }
	}

    private ListOfUserData cleanUsers(ListOfUserData list) {
        return new ListOfUserData(
                list.stream().map(this::cleanUser)
        );
    }


    private void getUser(RoutingContext routingContext) {
        final UsersApi api = UsersApi.get(routingContext, getUserStorage());
        final Promise<UserData> result = Promise.promise();
        result.future().setHandler(responseHandler(routingContext, this::cleanUser));

        try {
            final String identifier = routingContext.pathParam("identifier");

            // TODO forward business logic to UsersApi
            throw new IllegalStateException("not implemented");
        } catch(HttpError e) {
            result.fail(e);
        } catch (IllegalArgumentException e) {
            result.fail(new BadContentError(e));
        }
    }

    private void putUser(RoutingContext routingContext) {
        final UsersApi api = UsersApi.get(routingContext, getUserStorage());
        final Promise<UserData> result = Promise.promise();
        result.future().setHandler(responseHandler(routingContext, this::cleanUser));

        try {
            final UserData user = UserData.parse(routingContext.parsedHeaders().contentType().value(), routingContext.getBodyAsString());
            validateUserForPutUser(user);

            // TODO forward business logic to UsersApi
            throw new IllegalStateException("not implemented");
        } catch(HttpError e) {
            result.fail(e);
        } catch (IllegalArgumentException | IOException e) {
            result.fail(new BadContentError(e));
        }
    }

    private UserData cleanUser(UserData u) {
        return new UserData(u).setPassword(null).setLinkUrl(getSubPath(u.getUsername()));
    }

    private void validateUserForPutUser(UserData user) {
        requireAllAreNull(user.getId(), user.getLink(), user.getRole());
    }

}
