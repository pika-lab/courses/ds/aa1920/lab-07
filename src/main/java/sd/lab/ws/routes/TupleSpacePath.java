package sd.lab.ws.routes;

import io.vertx.core.Promise;
import io.vertx.ext.web.RoutingContext;
import sd.lab.ws.api.TupleSpaceApi;
import sd.lab.ws.exceptions.BadContentError;
import sd.lab.ws.exceptions.HttpError;
import sd.lab.ws.exceptions.InternalServerError;
import sd.lab.ws.presentation.*;
import sd.lab.ws.storage.TextualSpaceStorage;
import sd.lab.ws.storage.UserStorage;

import java.util.Optional;

import static sd.lab.ws.presentation.MIMETypes.*;

public class TupleSpacePath extends Path {

    private final TextualSpaceStorage tupleSpaceStorage;

    public TupleSpacePath(UserStorage userStorage, TextualSpaceStorage tsStorage) {
        super("/tuple-spaces", userStorage);
        tupleSpaceStorage = tsStorage;
    }

    @Override
	protected void setupRoutes() {
        // TODO setup routes
        throw new IllegalStateException("not implemented");
	}

    private void getTupleSpaces(RoutingContext routingContext) {
        final TupleSpaceApi api = TupleSpaceApi.get(routingContext, getUserStorage(), getTupleSpaceStorage());
        final Promise<ListOfLinkData> result = Promise.promise();
        // TODO implement cleaup op outputs

        try {
            // TODO implement validation of inputs and forward to API
            throw new IllegalStateException("not implemented");
        } catch(HttpError e) {
            result.fail(e);
        } catch (IllegalArgumentException e) {
            result.fail(new BadContentError(e));
        }
	}

    private ListOfLinkData cleanGetTupleSpacesInputsResult(ListOfLinkData links) {
        requireNoneIsNull(links);
        return cleanLinks(links);
    }

    private void validateGetTupleSpacesInputs(Optional<Integer> skip, Optional<Integer> limit, Optional<String> filter) {
        // always ok
    }

    private ListOfLinkData cleanLinks(ListOfLinkData links) {
        return new ListOfLinkData(
                links.stream().map(this::cleanLink)
        );
    }

    private LinkData cleanLink(LinkData link) {
        return new LinkData(getPath() + link.getUrl());
    }

    private void deleteTuple(RoutingContext routingContext) {
        final TupleSpaceApi api = TupleSpaceApi.get(routingContext, getUserStorage(), getTupleSpaceStorage());
        final Promise<TupleData> result = Promise.promise();
        // TODO implement cleaup op outputs

        try {
            // TODO implement validation of inputs and forward to API
            throw new IllegalStateException("not implemented");
        } catch(HttpError e) {
            result.fail(e);
        } catch (IllegalArgumentException e) {
            result.fail(new BadContentError(e));
        }
    }

    private void validateDeleteTupleInputs(String tupleSpaceName, Optional<String> template) {
        requireParameterIsPresent(template);
    }

    private TupleData cleanDeleteTupleResult(TupleData tuple) {
        requireNoneIsNull(tuple);
        return tuple;
    }

    private void getTuple(RoutingContext routingContext) {
        final TupleSpaceApi api = TupleSpaceApi.get(routingContext, getUserStorage(), getTupleSpaceStorage());
        final Promise<Data> result = Promise.promise();
        result.future().setHandler(responseHandler(routingContext, this::cleanGetTupleResult));

        try {
            final String tupleSpaceName = routingContext.pathParam("tupleSpaceName");
            final Optional<Boolean> count = Optional.ofNullable(routingContext.queryParams().get("count")).map(Boolean::parseBoolean);
            final Optional<String> template = Optional.ofNullable(routingContext.queryParams().get("template"));
            validateGetTupleInputs(tupleSpaceName, count, template);

            if (count.isPresent() && count.orElse(false)) {
                // TODO implement this case
                throw new IllegalStateException("not implemented");
            } else if(template.isPresent()) {
                // TODO implement this case
                throw new IllegalStateException("not implemented");
            } else {
                // TODO implement this case
                throw new IllegalStateException("not implemented");
            }
        } catch(HttpError e) {
            result.fail(e);
        } catch (IllegalArgumentException e) {
            result.fail(new BadContentError(e));
        }
    }

    private void validateGetTupleInputs(String tupleSpaceName, Optional<Boolean> count, Optional<String> template) {
        // always ok
    }

    private Data cleanGetTupleResult(Data x) {
        if (x instanceof NumberData) {
            NumberData n = (NumberData) x;
            if (n.getValue().longValue() >= 0) {
                return n;
            }
        } else if (x instanceof ListOfTupleData) {
            return x;
        } else if (x instanceof TupleData) {
            return x;
        }

        throw new InternalServerError();
    }

    private void postTuple(RoutingContext routingContext) {
        final TupleSpaceApi api = TupleSpaceApi.get(routingContext, getUserStorage(), getTupleSpaceStorage());
        final Promise<TupleData> result = Promise.promise();
        // TODO implement cleaup op outputs

        try {
            // TODO implement validation of inputs and forward to API
            throw new IllegalStateException("not implemented");
        } catch(HttpError e) {
            result.fail(e);
        } catch (IllegalArgumentException e) {
            result.fail(new BadContentError(e));
        }
    }

    private TupleData cleanPostTupleResult(TupleData x) {
        return cleanTuple(x);
    }

    private void validatePostTupleInputs(String tupleSpaceName, Optional<String> tuple, Optional<String> contentType) {
        requireStringIsNotBlank(tuple);
        requireStringIsNotBlank(contentType);
        requireOneOf(contentType.get(), APPLICATION_JSON, APPLICATION_XML, APPLICATION_YAML);
    }

    private TupleData cleanTuple(TupleData tuple) {
        requireNoneIsNull(tuple);
        return tuple;
    }

    public TextualSpaceStorage getTupleSpaceStorage() {
        return tupleSpaceStorage;
    }
}
