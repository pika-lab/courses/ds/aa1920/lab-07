package sd.lab.ws.routes;

import sd.lab.ws.storage.TextualSpaceStorage;
import sd.lab.ws.storage.UserStorage;

public class LindaPath extends Path {

    private final TextualSpaceStorage textualSpaceStorage = TextualSpaceStorage.getInstance();

	public LindaPath(String version) {
		super(
		        "/linda/v" + version,
                UserStorage.getInstance()
            );
	}

    @Override
    protected void setupRoutes() {
        append(new UsersPath(getUserStorage()));
        append(new TupleSpacePath(getUserStorage(), getTextualSpaceStorage()));
    }

    public TextualSpaceStorage getTextualSpaceStorage() {
        return textualSpaceStorage;
    }
}
