package sd.lab.ws.api;

import io.vertx.core.Promise;
import io.vertx.ext.web.RoutingContext;
import sd.lab.linda.textual.RegexTemplate;
import sd.lab.linda.textual.StringTuple;
import sd.lab.ws.presentation.ListOfLinkData;
import sd.lab.ws.presentation.ListOfTupleData;
import sd.lab.ws.presentation.NumberData;
import sd.lab.ws.presentation.TupleData;
import sd.lab.ws.storage.TextualSpaceStorage;
import sd.lab.ws.storage.UserStorage;

public interface TupleSpaceApi extends Api {

    void getAllTuples(String tupleSpaceName, Promise<? super ListOfTupleData> promise);

    void countAllTuples(String tupleSpaceName, Promise<? super NumberData> promise);

    void insertTuple(String tupleSpaceName, StringTuple tuple, Promise<? super TupleData> promise);

    void consumeTuple(String tupleSpaceName, RegexTemplate template, Promise<? super TupleData> promise);

    void observeTuple(String tupleSpaceName, RegexTemplate template, Promise<? super TupleData> promise);

    void getAllTupleSpaces(Integer skip, Integer limit, String filter, Promise<? super ListOfLinkData> promise);

    static TupleSpaceApi get(RoutingContext context, UserStorage userStorage, TextualSpaceStorage tsStorage) {
        return new TupleSpaceApiImpl(context, userStorage, tsStorage);
    }
}