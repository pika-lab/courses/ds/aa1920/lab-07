package sd.lab.ws.api;

import io.vertx.core.Promise;
import io.vertx.ext.web.RoutingContext;
import sd.lab.linda.textual.RegexTemplate;
import sd.lab.linda.textual.StringTuple;
import sd.lab.linda.textual.TextualSpace;
import sd.lab.ws.presentation.*;
import sd.lab.ws.storage.TextualSpaceStorage;
import sd.lab.ws.storage.UserStorage;

public class TupleSpaceApiImpl extends AbstractApi implements TupleSpaceApi {
    
    private final TextualSpaceStorage textualSpaceStorage;
    
    TupleSpaceApiImpl(RoutingContext routingContext, UserStorage userStorage, TextualSpaceStorage tsStorage) {
        super(routingContext, userStorage);
        textualSpaceStorage = tsStorage;
    }

    @Override
    public void getAllTuples(String tupleSpaceName, Promise<? super ListOfTupleData> promise) {
        final TextualSpace ts = getTextualSpaceStorage().createIfAbsentOrGet(tupleSpaceName);

        // TODO implement business logic
        throw new IllegalStateException("not implemented");
    }

    @Override
    public void countAllTuples(String tupleSpaceName, Promise<? super NumberData> promise) {
        final TextualSpace ts = getTextualSpaceStorage().createIfAbsentOrGet(tupleSpaceName);
        ts.getSize().whenComplete((count, error) -> {
            if (error != null) {
                promise.fail(error);
            } else {
                promise.complete(new NumberData(count));
            }
        });
    }

    @Override
    public void insertTuple(String tupleSpaceName, StringTuple tuple, Promise<? super TupleData> promise) {
        final TextualSpace ts = getTextualSpaceStorage().createIfAbsentOrGet(tupleSpaceName);

        // TODO implement business logic
        throw new IllegalStateException("not implemented");
    }

    @Override
    public void consumeTuple(String tupleSpaceName, RegexTemplate template, Promise<? super TupleData> promise) {
        final TextualSpace ts = getTextualSpaceStorage().createIfAbsentOrGet(tupleSpaceName);

        // TODO implement business logic
        throw new IllegalStateException("not implemented");
    }

    @Override
    public void observeTuple(String tupleSpaceName, RegexTemplate template, Promise<? super TupleData> promise) {
        final TextualSpace ts = getTextualSpaceStorage().createIfAbsentOrGet(tupleSpaceName);

        // TODO implement business logic
        throw new IllegalStateException("not implemented");
    }

    @Override
    public void getAllTupleSpaces(Integer skip, Integer limit, String filter, Promise<? super ListOfLinkData> promise) {
        final ListOfLinkData result = new ListOfLinkData(
                getTextualSpaceStorage().getAll()
                        .map(TextualSpace::getName)
                        .filter(name -> name.contains(filter))
                        .skip(skip)
                        .limit(limit)
                        .map(name -> "/" + name) // the reminder of the path will be injected by the route
                        .map(LinkData::new)
        );

        promise.complete(result);
    }

    public TextualSpaceStorage getTextualSpaceStorage() {
        return textualSpaceStorage;
    }
}
