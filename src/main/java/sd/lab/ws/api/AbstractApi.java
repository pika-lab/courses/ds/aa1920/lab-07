package sd.lab.ws.api;

import io.vertx.ext.web.RoutingContext;
import sd.lab.ws.storage.UserStorage;

import java.util.Objects;

abstract class AbstractApi implements Api {
    private final RoutingContext routingContext;
    private final UserStorage userStorage;

    AbstractApi(RoutingContext routingContext, UserStorage userStorage) {
        this.routingContext = Objects.requireNonNull(routingContext);
        this.userStorage = Objects.requireNonNull(userStorage);
    }

    @Override
    public RoutingContext getRoutingContext() {
        return routingContext;
    }

    public UserStorage getUserStorage() {
        return userStorage;
    }
}
