package sd.lab.ws.api;

import io.vertx.core.Promise;
import io.vertx.ext.web.RoutingContext;
import sd.lab.ws.presentation.LinkData;
import sd.lab.ws.presentation.ListOfUserData;
import sd.lab.ws.presentation.UserData;
import sd.lab.ws.storage.UserStorage;

public interface UsersApi extends Api {

    void createUser(UserData userData, Promise<? super LinkData> promise);

    void readAllUsers(Integer skip, Integer limit, String filter, Promise<? super ListOfUserData> promise);

    void readUser(String identifier, Promise<? super UserData> promise);

    void updateUser(String identifier, UserData newUserData, Promise<? super UserData> promise);

    static UsersApi get(RoutingContext context, UserStorage storage) {
        return new UserApiImpl(context, storage);
    }
}