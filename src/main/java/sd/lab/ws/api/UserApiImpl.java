package sd.lab.ws.api;

import io.vertx.core.Promise;
import io.vertx.ext.web.RoutingContext;
import sd.lab.ws.exceptions.ConflictError;
import sd.lab.ws.exceptions.ForbiddenError;
import sd.lab.ws.exceptions.NotFoundError;
import sd.lab.ws.presentation.LinkData;
import sd.lab.ws.presentation.ListOfUserData;
import sd.lab.ws.presentation.UserData;
import sd.lab.ws.storage.UserStorage;

import java.util.Optional;
import java.util.stream.Stream;

class UserApiImpl extends AbstractApi implements UsersApi {

    UserApiImpl(RoutingContext routingContext, UserStorage storage) {
        super(routingContext, storage);
    }

    @Override
    public void createUser(UserData userData, Promise<? super LinkData> promise) {
        try {
            // non-admins cannot register admins
            if (!getAuthenticatedUser().isPresent() || getAuthenticatedUser().get().getRole() == UserData.Role.USER) {
                userData.setRole(UserData.Role.USER);
            }

            // mimic access to DB
            getUserStorage().register(new UserData(userData));

            // this assumes the stub fill populate the whole path
            promise.complete(new LinkData("/" + userData.getUsername()));
        } catch (IllegalArgumentException e) {
            throw new ConflictError(e);
        }
    }

    @Override
    public void readAllUsers(Integer skip, Integer limit, String filter, Promise<? super ListOfUserData> promise) {
        ensureAuthenticatedUserAtLeast(UserData.Role.ADMIN);

        // beyond this point, we're sure that the client is at least an admin

        // TODO implement business logic
        throw new IllegalStateException("not implemented");
    }

    @Override
    public void readUser(String identifier, Promise<? super UserData> promise) {
        ensureAuthenticatedUserAtLeast(UserData.Role.USER);

        // beyond this point, we're sure that the client is at least a user

        final Optional<UserData> user = getUserStorage().findByIdentifier(identifier);
        if (user.isPresent()) {
            // TODO implement business logic
            throw new IllegalStateException("not implemented");
        } else {
            // TODO implement business logic
            throw new IllegalStateException("not implemented");
        }
    }

    @Override
    public void updateUser(String identifier, UserData newUserData, Promise<? super UserData> promise) {
        ensureAuthenticatedUserAtLeast(UserData.Role.USER);

        getAuthenticatedUser().ifPresent(u -> {
            if (!u.isIdentifiedBy(identifier)) { // user data can only be updated by its owner
                throw new ForbiddenError();
            }
        });

        // TODO implement business logic (use mergeUsers)
        throw new IllegalStateException("not implemented");
    }

    private UserData mergeUsers(UserData currentUser, UserData newUserData) {
        final UserData edits = new UserData(newUserData).setId(null);
        final UserData result = new UserData(currentUser).setPropertiesToNonNullsOf(edits);
        return result;
    }


}
