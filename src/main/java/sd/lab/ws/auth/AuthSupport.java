package sd.lab.ws.auth;

import io.vertx.core.http.HttpHeaders;
import io.vertx.ext.web.RoutingContext;
import sd.lab.ws.exceptions.ForbiddenError;
import sd.lab.ws.exceptions.UnauthorizedError;
import sd.lab.ws.presentation.UserData;
import sd.lab.ws.storage.UserStorage;

import java.io.IOException;
import java.util.Optional;

public interface AuthSupport {

    UserStorage getUserStorage();

    default String getAuthorizationHeader(RoutingContext routingContext) {
        return routingContext.request().getHeader(HttpHeaders.AUTHORIZATION);
    }

    default Optional<UserData> getAuthenticatedUser(RoutingContext routingContext) {
        final Optional<UserData> credentials;
        if (getAuthorizationHeader(routingContext) != null) {
            UserData result;
            try {
                result = UserData.fromJSON(getAuthorizationHeader(routingContext));
            } catch (IOException e) {
                try {
                    result = UserData.fromYAML(getAuthorizationHeader(routingContext));
                } catch (IOException e1) {
                    try {
                        result = UserData.fromXML(getAuthorizationHeader(routingContext));
                    } catch (IOException e2) {
                        result = null;
                    }
                }
            }
            credentials = Optional.ofNullable(result);
            if (credentials.isPresent()) {
                final Optional<UserData> user = getUserStorage().find(credentials.get());
                if (!user.isPresent() || !user.get().getPassword().equals(credentials.get().getPassword())) {
                    throw new UnauthorizedError("Impossible to authenticate the user");
                }
                return user;
            }
        } else {
            credentials = Optional.empty();
        }

        return credentials;
    }

    default boolean isAuthenticatedUserAtLeast(RoutingContext routingContext, UserData.Role role) {
        return getAuthenticatedUser(routingContext).isPresent()
                && getAuthenticatedUser(routingContext).get().getRole().compareTo(role) >= 0;
    }

    default void ensureAuthenticatedUserAtLeast(RoutingContext routingContext, UserData.Role role) {
        if (!getAuthenticatedUser(routingContext).isPresent()) {
            throw new UnauthorizedError();
        }
        if (getAuthenticatedUser(routingContext).get().getRole().compareTo(role) < 0) {
            throw new ForbiddenError();
        }
    }
}
