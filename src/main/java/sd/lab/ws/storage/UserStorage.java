package sd.lab.ws.storage;

import sd.lab.ws.presentation.UserData;

import java.util.*;
import java.util.stream.Stream;

public interface UserStorage extends Storage {

    Optional<UserData> find(UserData minimal);

    default Optional<UserData> findByUUID(UUID id) {
        return find(new UserData().setId(id));
    }

    default Optional<UserData> findByUsername(String username) {
        return find(new UserData().setUsername(username));
    }

    default Optional<UserData> findByEmail(String email) {
        return find(new UserData().setEmail(email));
    }

    Optional<UserData> findByIdentifier(String identifier);

    UserData register(UserData newUser) throws IllegalArgumentException;

    UserData unregister(UserData existingUser) throws IllegalArgumentException;

    UserData update(UserData existingUser) throws IllegalArgumentException;

    Stream<UserData> getAll();

    static UserStorage getInstance() {
        return new UserStorageImpl();
    }
}
