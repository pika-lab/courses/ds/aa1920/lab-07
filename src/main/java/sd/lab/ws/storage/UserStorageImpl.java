package sd.lab.ws.storage;

import sd.lab.ws.presentation.UserData;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Stream;

public class UserStorageImpl implements UserStorage {

    private final Set<UserData> users = new HashSet<>();

    public UserStorageImpl() {
        // add some default user
        users.add(
                new UserData()
                    .setRole(UserData.Role.ADMIN)
                    .setUsername("admin")
                    .setFullName("Administrator")
                    .setEmail("tuplespace.administrator@unibo.it")
                    .setPassword("admin")
                    .setId(UUID.randomUUID())
        );

        users.add(
                new UserData()
                        .setRole(UserData.Role.USER)
                        .setUsername("user")
                        .setFullName("User")
                        .setEmail("tuplespace.user@unibo.it")
                        .setPassword("user")
                        .setId(UUID.randomUUID())
        );
    }

    @Override
    public Optional<UserData> find(UserData minimal) {
        return users.stream().filter(it -> it.sameUserOf(minimal)).findAny();
    }

    @Override
    public Optional<UserData> findByIdentifier(String identifier) {
        return users.stream().filter(it -> it.isIdentifiedBy(identifier)).findAny();
    }

    @Override
    public UserData register(UserData newUser) {
        final Optional<UserData> existing = find(newUser);
        if (existing.isPresent()) {
            throw new IllegalArgumentException(
                    String.format(
                        "Some identifier of the new user %s conflicts with some identifier of the already registered user %s",
                        newUser,
                        existing.get()
                    )
            );
        }
        users.add(newUser);
        return newUser;
    }

    @Override
    public UserData unregister(UserData existingUser) {
        final Optional<UserData> existing = find(existingUser);
        if (!existing.isPresent()) {
            throw new IllegalArgumentException(
                    String.format(
                            "No registered user shares its identifiers with %s",
                            existingUser
                    )
            );
        }
        users.remove(existing.get());
        return existing.get();
    }

    @Override
    public UserData update(UserData existingUser) {
        final Optional<UserData> existing = find(existingUser);
        if (!existing.isPresent()) {
            throw new IllegalArgumentException(
                    String.format(
                            "No registered user shares its identifiers with %s",
                            existingUser
                    )
            );
        }
        users.remove(existing.get());
        users.add(existingUser);
        return existingUser;
    }

    @Override
    public Stream<UserData> getAll() {
        return users.stream();
    }
}
