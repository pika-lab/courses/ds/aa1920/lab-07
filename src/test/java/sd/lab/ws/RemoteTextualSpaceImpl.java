package sd.lab.ws;

import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import org.apache.commons.collections4.KeyValue;
import org.apache.commons.collections4.MultiSet;
import org.apache.commons.collections4.keyvalue.DefaultKeyValue;
import org.apache.commons.collections4.multiset.HashMultiSet;
import sd.lab.linda.textual.RegexTemplate;
import sd.lab.linda.textual.StringTuple;
import sd.lab.linda.textual.TextualSpace;
import sd.lab.ws.presentation.*;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

public class RemoteTextualSpaceImpl implements TextualSpace {
    private static final Logger LOGGER = LoggerFactory.getLogger(TestUsers.class);
    private static final String BASE_URL = "/linda/v1";
    private static final String NO_HEADER = null;
    public static class HttpStatusError extends IllegalStateException {

        private final int statusCode;
        public HttpStatusError(int statusCode) {
            this.statusCode = statusCode;
        }

        public HttpStatusError(int statusCode, String message) {
            super(message);
            this.statusCode = statusCode;
        }

        public HttpStatusError(int statusCode, String message, Throwable cause) {
            super(message, cause);
            this.statusCode = statusCode;
        }

        public HttpStatusError(int statusCode, Throwable cause) {
            super(cause);
            this.statusCode = statusCode;
        }

        public int getStatusCode() {
            return statusCode;
        }

    }
    public static class UnmarshallingError extends IllegalStateException {

        private final String raw;
        private final String mimeType;
        private final Class<? extends Data> clazz;
        public UnmarshallingError(String raw, String mimeType, Class<? extends Data> clazz) {
            this.raw = raw;
            this.mimeType = mimeType;
            this.clazz = clazz;
        }

        public UnmarshallingError(String s, String raw, String mimeType, Class<? extends Data> clazz) {
            super(s);
            this.raw = raw;
            this.mimeType = mimeType;
            this.clazz = clazz;
        }

        public UnmarshallingError(String message, Throwable cause, String raw, String mimeType, Class<? extends Data> clazz) {
            super(message, cause);
            this.raw = raw;
            this.mimeType = mimeType;
            this.clazz = clazz;
        }

        public UnmarshallingError(Throwable cause, String raw, String mimeType, Class<? extends Data> clazz) {
            super(cause);
            this.raw = raw;
            this.mimeType = mimeType;
            this.clazz = clazz;
        }

        public String getRaw() {
            return raw;
        }

        public String getMimeType() {
            return mimeType;
        }

        public Class<? extends Data> getClazz() {
            return clazz;
        }

    }

    private final HttpClient client;
    private final String host;
    private final int port;
    private final String name;
    private final String mimeType;
    private final UserData credentials;


    public RemoteTextualSpaceImpl(HttpClient client, String host, int port, String name, String mimeType, String username, String password) {
        this.client = Objects.requireNonNull(client);
        this.host = Objects.requireNonNull(host);
        this.port = port;
        this.name = Objects.requireNonNull(name);
        this.mimeType = Objects.requireNonNull(mimeType);
        this.credentials = new UserData().setUsername(username).setPassword(password);
    }

    private static KeyValue<String, Object> query(String key, Object value) {
        return new DefaultKeyValue<>(key, value);
    }

    private static String urlEncode(String x) {
        try {
            return URLEncoder.encode(x, "UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new IllegalStateException(e);
        }
    }

    private HttpClientRequest request(HttpMethod method, UserData credentials, String contentType, String accept, KeyValue<String, Object>... queries) {
        final String path = "/tuple-spaces/" + name;
        final String queryString = Arrays.stream(queries)
                .map(kv -> kv.getKey() + "=" + urlEncode(kv.getValue().toString()))
                .collect(Collectors.joining("&", "?", ""));
        HttpClientRequest temp = client.request(method, port, host, BASE_URL + path + queryString);
        temp = temp.connectionHandler(conn -> LOGGER.debug("Request {0} {1}", method, "http://" + host + ":" + port + BASE_URL + path + queryString));
        if (credentials != null) {
            temp = temp.putHeader("Authorization", new UserData().setUsername(credentials.getUsername()).setPassword(credentials.getPassword()).toJSONString());
        }
        if (contentType != null) {
            temp = temp.putHeader("Content-Type", contentType);
        }
        if (accept != null) {
            temp = temp.putHeader("Accept", accept);
        }
        return temp;
    }

    @Override
    public CompletableFuture<StringTuple> rd(RegexTemplate template) {
        final CompletableFuture<StringTuple> result = new CompletableFuture<>();
        request(HttpMethod.GET, credentials, NO_HEADER, mimeType, query("template", template.getRegex().pattern()))
                .handler(res -> {
                    if (res.statusCode() == 200) {
                        res.bodyHandler(body -> {
                            try {
                                TupleData tupleData = TupleData.parse(mimeType, body.toString());
                                result.complete(tupleData.toTuple());
                            } catch (IOException e) {
                                result.completeExceptionally(new UnmarshallingError(body.toString(), mimeType, TupleData.class));
                            }
                        });
                    } else {
                        result.completeExceptionally(new HttpStatusError(res.statusCode()));
                    }
                })
                .end();
        return result;
    }

    @Override
    public CompletableFuture<StringTuple> in(RegexTemplate template) {
        final CompletableFuture<StringTuple> result = new CompletableFuture<>();
        request(HttpMethod.DELETE, credentials, NO_HEADER, mimeType, query("template", template.getRegex().pattern()))
                .handler(res -> {
                    if (res.statusCode() == 200) {
                        res.bodyHandler(body -> {
                            try {
                                TupleData tupleData = TupleData.parse(mimeType, body.toString());
                                result.complete(tupleData.toTuple());
                            } catch (IOException e) {
                                result.completeExceptionally(new UnmarshallingError(body.toString(), mimeType, TupleData.class));
                            }
                        });
                    } else {
                        result.completeExceptionally(new HttpStatusError(res.statusCode()));
                    }
                })
                .end();
        return result;
    }

    @Override
    public CompletableFuture<StringTuple> out(StringTuple tuple) {
        final CompletableFuture<StringTuple> result = new CompletableFuture<>();
        request(HttpMethod.POST, credentials, mimeType, mimeType)
                .handler(res -> {
                    if (res.statusCode() == 200) {
                        res.bodyHandler(body -> {
                            try {
                                TupleData tupleData = TupleData.parse(mimeType, body.toString());
                                result.complete(tupleData.toTuple());
                            } catch (IOException e) {
                                result.completeExceptionally(new UnmarshallingError(body.toString(), mimeType, TupleData.class));
                            }
                        });
                    } else {
                        result.completeExceptionally(new HttpStatusError(res.statusCode()));
                    }
                })
                .end(new TupleData(tuple).toMIMETypeString(mimeType));
        return result;
    }

    @Override
    public CompletableFuture<MultiSet<? extends StringTuple>> get() {
        final CompletableFuture<MultiSet<? extends StringTuple>> result = new CompletableFuture<>();
        request(HttpMethod.GET, credentials, NO_HEADER, mimeType)
                .handler(res -> {
                    if (res.statusCode() == 200) {
                        res.bodyHandler(body -> {
                            try {
                                ListOfTupleData tuplesData = ListOfTupleData.parse(mimeType, body.toString());
                                result.complete(new HashMultiSet<>(tuplesData.toListOfTuples()));
                            } catch (IOException e) {
                                result.completeExceptionally(new UnmarshallingError(body.toString(), mimeType, ListOfTupleData.class));
                            }
                        });
                    } else if (res.statusCode() == 204) {
                        result.complete(new HashMultiSet<>());
                    } else {
                        result.completeExceptionally(new HttpStatusError(res.statusCode()));
                    }
                })
                .end();
        return result;
    }

    @Override
    public CompletableFuture<Integer> getSize() {
        final CompletableFuture<Integer> result = new CompletableFuture<>();
        request(HttpMethod.GET, credentials, NO_HEADER, mimeType, query("count", true))
                .handler(res -> {
                    if (res.statusCode() == 200) {
                        res.bodyHandler(body -> {
                            try {
                                NumberData tuplesData = NumberData.parse(mimeType, body.toString());
                                result.complete(tuplesData.getValue().intValue());
                            } catch (IOException e) {
                                result.completeExceptionally(new UnmarshallingError(body.toString(), mimeType, NumberData.class));
                            }
                        });
                    } else {
                        result.completeExceptionally(new HttpStatusError(res.statusCode()));
                    }
                })
                .end();
        return result;
    }

    @Override
    public String getName() {
        return name;
    }
}
