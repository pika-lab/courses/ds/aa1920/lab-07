package sd.lab.ws;

import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import io.vertx.core.http.HttpClientRequest;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunnerWithParametersFactory;
import org.apache.commons.collections4.KeyValue;
import org.apache.commons.collections4.keyvalue.DefaultKeyValue;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sd.lab.ws.presentation.Data;
import sd.lab.ws.presentation.LinkData;
import sd.lab.ws.presentation.UserData;

import java.time.Duration;
import java.util.Arrays;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Collectors;

import static io.vertx.core.http.HttpMethod.*;
import static sd.lab.ws.presentation.MIMETypes.*;

@RunWith(Parameterized.class)
@Parameterized.UseParametersRunnerFactory(VertxUnitRunnerWithParametersFactory.class)
public class TestUsers {
    protected static final Logger LOGGER = LoggerFactory.getLogger(TestUsers.class);
    private static final Duration MAX_WAIT_DURATION = Duration.ofSeconds(5);
    private static final long MAX_WAIT = MAX_WAIT_DURATION.toMillis();

    private static final String BASE_URL = "/linda/v1";

    private static final UserData NO_CRED = null;
    private static final UserData ADMIN_CRED = new UserData().setUsername("admin").setPassword("admin");
    private static final UserData USER_CRED = new UserData().setUsername("user").setPassword("user");

    private static final String NO_HEADER = null;
    private static final Data NO_BODY = null;

    @Parameterized.Parameters
    public static Iterable<String> data() {
        return Arrays.asList(APPLICATION_JSON, APPLICATION_XML, APPLICATION_YAML);
    }

    private static Vertx vertx;

    @BeforeClass
    public static void setUpClass() {
        vertx = Vertx.vertx();
    }

    private Service service;
    private HttpClient client;

    @Before
    public void setUp() throws InterruptedException, ExecutionException, TimeoutException {
        service = Service.start(vertx).awaitDeployment(MAX_WAIT_DURATION);

        client = vertx.createHttpClient(
                new HttpClientOptions()
                        .setMaxPoolSize(1)
                        .setHttp2MaxPoolSize(1)
        );
    }

    @After
    public void tearDown() throws Exception {
        service.stop();
        service.awaitTermination(MAX_WAIT_DURATION);
        client.close();
    }


    private final String mimeType;

    public TestUsers(String mimeType) {
        this.mimeType = mimeType;
    }

    private static KeyValue<String, Object> query(String key, Object value) {
        return new DefaultKeyValue<>(key, value);
    }

    private HttpClientRequest request(HttpMethod method, String path, UserData credentials, String contentType, String accept, KeyValue<String, Object>... queries) {
        final String queryString = Arrays.stream(queries)
                .map(kv -> kv.getKey() + "=" + kv.getValue().toString())
                .collect(Collectors.joining("&", "?", ""));
        HttpClientRequest temp = client.request(method, 8080, "localhost", BASE_URL + path + queryString);
        temp = temp.connectionHandler(conn -> LOGGER.debug("Request {0} {1}", method, BASE_URL + path + queryString));
        if (credentials != null) {
            temp = temp.putHeader("Authorization", new UserData().setUsername(credentials.getUsername()).setPassword(credentials.getPassword()).toJSONString());
        }
        if (contentType != null) {
            temp = temp.putHeader("Content-Type", contentType);
        }
        if (accept != null) {
            temp = temp.putHeader("Accept", accept);
        }
        return temp;
    }

    private void getUserFromLinkAsync(String link, Consumer<UserData> continuation) {
        final Future<UserData> futureResult = Future.future();
        final Function<UserData, Void> actualContinuation = user -> {
            continuation.accept(user);
            return null;
        };
        futureResult.otherwise(e -> null).map(actualContinuation);

        request(GET, link.substring(link.indexOf("/users")) , ADMIN_CRED, APPLICATION_JSON, APPLICATION_JSON)
                .handler(res -> {
                    if (res.statusCode() != 200) {
                        futureResult.fail(new IllegalStateException());
                    } else res.bodyHandler(body -> {
                        try {
                            final UserData user = UserData.parse(APPLICATION_JSON, body.toString());
                            futureResult.complete(user);
                        } catch (Throwable e) {
                            futureResult.fail(e);
                        }
                    });
                }).end();
    }

    @Test
    public void testAdminsCanAddAdmins(TestContext context) {
        Async async = context.async();

        final String suffix = mimeType.substring(mimeType.indexOf("/") + 1);

        final UserData newUser = new UserData()
                .setUsername("user-" + suffix)
                .setEmail(String.format("user.%s@email.com", suffix))
                .setPassword("password-" + suffix)
                .setRole(UserData.Role.ADMIN);

        request(POST, "/users", ADMIN_CRED, mimeType, mimeType)
                .handler(res -> {
                    res.bodyHandler(body -> {
                        try {
                            context.assertEquals(200, res.statusCode());
                            final LinkData result = LinkData.parse(mimeType, body.toString());
                            context.assertEquals(
                                    BASE_URL + "/users/" + newUser.getUsername(),
                                    result.getUrl()
                            );

                            getUserFromLinkAsync(result.getUrl(), u -> {
                                try {
                                    context.assertEquals(UserData.Role.ADMIN, u.getRole());
                                } finally {
                                    async.complete();
                                }
                            });

                        } catch (Throwable e) {
                            context.fail(e);
                            async.complete();
                        }
                    });
                }).end(
                        newUser.toMIMETypeString(mimeType)
                );

        async.await(MAX_WAIT);
    }

    @Test
    public void testNotAllUsersExists(TestContext context) {
        Async async = context.async();

        request(GET, "/users/non-existing-user", USER_CRED, NO_HEADER, mimeType)
                .handler(res -> {
                    res.bodyHandler(body -> {
                        try {
                            context.assertEquals(404, res.statusCode());

                        } catch (Throwable e) {
                            context.fail(e);
                        } finally {
                            async.complete();
                        }
                    });
                }).end();

        async.await(MAX_WAIT);
    }

    @Test
    public void testCannotDeleteNonExistingUsers(TestContext context) {
        Async async = context.async();

        request(DELETE, "/users/non-existing-user", ADMIN_CRED, NO_HEADER, NO_HEADER)
                .handler(res -> {
                    res.bodyHandler(body -> {
                        try {
                            context.assertEquals(405, res.statusCode());

                        } catch (Throwable e) {
                            context.fail(e);
                        } finally {
                            async.complete();
                        }
                    });
                }).end();

        async.await(MAX_WAIT);
    }

    @Test
    public void testAUserCanEditItsData(TestContext context) {
        Async async = context.async();

        request(PUT, "/users/user", USER_CRED, mimeType, mimeType)
                .handler(res -> {
                    try {
                        context.assertEquals(200, res.statusCode());
                        res.bodyHandler(body -> {
                            try {
                                final UserData newUserData = UserData.parse(mimeType, body.toString());
                                context.assertEquals("new-username", newUserData.getUsername());
                                context.assertEquals("new.email@studio.unibo.it", newUserData.getEmail());
                                context.assertTrue(newUserData.getId() != null);
                                context.assertEquals(new LinkData(BASE_URL + "/users/new-username"), newUserData.getLink());
                                context.assertEquals("User", newUserData.getFullName());
                            } catch (Throwable e) {
                                context.fail(e);
                            } finally {
                                async.complete();
                            }
                        });
                    } catch (Throwable e) {
                        context.fail(e);
                        async.complete();
                    }
                }).end(
                    new UserData()
                            .setUsername("new-username")
                            .setEmail("new.email@studio.unibo.it")
                            .toMIMETypeString(mimeType)
                );

        async.await(MAX_WAIT);
    }

    @Test
    public void testUsersCannotAddAdmins(TestContext context) {
        Async async = context.async();

        final String suffix = mimeType.substring(mimeType.indexOf("/") + 1);

        final UserData newUser = new UserData()
                .setUsername("user-" + suffix)
                .setEmail(String.format("user.%s@email.com", suffix))
                .setPassword("password-" + suffix)
                .setRole(UserData.Role.ADMIN);

        request(POST, "/users", USER_CRED, mimeType, mimeType)
                .handler(res -> {
                    res.bodyHandler(body -> {
                        try {
                            context.assertEquals(200, res.statusCode());
                            final LinkData result = LinkData.parse(mimeType, body.toString());
                            context.assertEquals(
                                    BASE_URL + "/users/" + newUser.getUsername(),
                                    result.getUrl()
                            );

                            getUserFromLinkAsync(result.getUrl(), u -> {
                                try {
                                    context.assertEquals(UserData.Role.USER, u.getRole());
                                } catch (Exception e) {
                                    context.fail(e);
                                } finally {
                                    async.complete();
                                }
                            });

                        } catch (Throwable e) {
                            context.fail(e);
                        } finally {
                            async.complete();
                        }
                    });
                }).end(
                        newUser.toMIMETypeString(mimeType)
                );

        async.await(MAX_WAIT);
    }

    @Test
    public void testUnloggedCannotAddAdmins(TestContext context) {
        Async async = context.async();

        final String suffix = mimeType.substring(mimeType.indexOf("/") + 1);

        final UserData newUser = new UserData()
                .setUsername("user-" + suffix)
                .setEmail(String.format("user.%s@email.com", suffix))
                .setPassword("password-" + suffix)
                .setRole(UserData.Role.ADMIN);

        request(POST, "/users", NO_CRED, mimeType, mimeType)
                .handler(res -> {
                    res.bodyHandler(body -> {
                        try {
                            context.assertEquals(200, res.statusCode());
                            final LinkData result = LinkData.parse(mimeType, body.toString());
                            context.assertEquals(
                                    BASE_URL + "/users/" + newUser.getUsername(),
                                    result.getUrl()
                            );

                            getUserFromLinkAsync(result.getUrl(), u -> {
                                try {
                                    context.assertEquals(UserData.Role.USER, u.getRole());
                                } catch (Exception e) {
                                    context.fail(e);
                                } finally {
                                    async.complete();
                                }
                            });

                        } catch (Throwable e) {
                            context.fail(e);
                            async.complete();
                        }
                    });
                }).end(
                        newUser.toMIMETypeString(mimeType)
                );

        async.await(MAX_WAIT);
    }


    @Test
    public void testGetUsersRequiresAuth(TestContext context) {
        Async async = context.async();

        request(GET, "/users", NO_CRED, NO_HEADER, NO_HEADER)
                .handler(res -> {
                    try {
                        context.assertEquals(401, res.statusCode());
                    } finally {
                        async.complete();
                    }
                }).end();

        async.await(MAX_WAIT);
    }

    @Test
    public void testGetUserRequiresAuth(TestContext context) {
        Async async = context.async();

        request(GET, "/users/admin", NO_CRED, NO_HEADER, NO_HEADER)
                .handler(res -> {
                    try {
                        context.assertEquals(401, res.statusCode());
                    } finally {
                        async.complete();
                    }
                }).end();

        async.await(MAX_WAIT);
    }

    @Test
    public void testPutUserRequiresAuth(TestContext context) {
        Async async = context.async();

        request(PUT, "/users/admin", NO_CRED, mimeType, APPLICATION_ANY)
                .handler(res -> {
                    try {
                        context.assertEquals(401, res.statusCode());
                    } finally {
                        async.complete();
                    }
                }).end(
                        new UserData().setUsername("whatever").toMIMETypeString(mimeType)
                );

        async.await(MAX_WAIT);
    }

}
