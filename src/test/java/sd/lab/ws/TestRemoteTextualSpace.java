package sd.lab.ws;

import io.vertx.core.Vertx;
import io.vertx.core.http.HttpClient;
import io.vertx.core.http.HttpClientOptions;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import sd.lab.linda.textual.AbstractTestTextualSpace;
import sd.lab.linda.textual.TextualSpace;
import sd.lab.ws.presentation.MIMETypes;

import java.time.Duration;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RunWith(Parameterized.class)
public class TestRemoteTextualSpace extends AbstractTestTextualSpace {

    private static final Duration MAX_WAIT = Duration.ofSeconds(5);

    @BeforeClass
    public static void setUpUnit() throws Exception {
        AbstractTestTextualSpace.setUpUnit();

        service = Service.start(Vertx.vertx()).awaitDeployment(MAX_WAIT);

        client = Vertx.vertx().createHttpClient(
                new HttpClientOptions()
                        .setMaxPoolSize(1 << 15)
                        .setHttp2MaxPoolSize(1 << 15)
        );
    }

    @Parameterized.Parameters
    public static Iterable<String> data() {
        return Stream.of(MIMETypes.APPLICATION_JSON, MIMETypes.APPLICATION_JSON, MIMETypes.APPLICATION_JSON)
                .collect(Collectors.toList());
    }

    private final String mimeType;
    private static Service service;
    private static HttpClient client;
    private static int index;

    public TestRemoteTextualSpace(String mimeType) {
        this.mimeType = mimeType;
    }

    @Override
    public void setUp() throws Exception {
//        service = Service.start(vertx).awaitDeployment();
//        client = vertx.createHttpClient(
//                new HttpClientOptions()
//                        .setMaxPoolSize(1 << 15)
//                        .setHttp2MaxPoolSize(1 << 15)
//        );
        super.setUp();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        service.stop();
        service.awaitTermination(MAX_WAIT);
        client.close();
    }

    @Override
    protected TextualSpace getTupleSpace(ExecutorService executor) {
        executor.shutdown();
        return new RemoteTextualSpaceImpl(client, "localhost", 8080, "default-" + mimeType.replace("/", "-") + "-" + index++, mimeType, "admin", "admin");
    }
}
