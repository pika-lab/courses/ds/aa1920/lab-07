plugins {
    java
    `java-library`
}

repositories {
    mavenCentral()
}

group = "sd.lab"
version = "1.0-SNAPSHOT"

dependencies {
    api("org.apache.commons", "commons-collections4", "4.2")
    api("io.vertx", "vertx-web", "3.8.3")

    implementation("com.fasterxml.jackson.core", "jackson-core", "2.10.1")
    implementation("com.fasterxml.jackson.datatype", "jackson-datatype-jsr310", "2.10.1")
    implementation("com.fasterxml.jackson.dataformat", "jackson-dataformat-xml", "2.10.1")
    implementation("com.fasterxml.jackson.dataformat", "jackson-dataformat-yaml", "2.10.1")

    testImplementation("junit", "junit", "4.12")
    testImplementation("ch.qos.logback", "logback-classic", "1.2.3")
    testImplementation("io.vertx", "vertx-web-client", "3.8.3")
    testImplementation("io.vertx", "vertx-unit", "3.8.3")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
    targetCompatibility = JavaVersion.VERSION_1_8
}